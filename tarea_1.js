// Juan Luis Del Valle Sotelo - 338912


// En esta tarea vamos a utilizar una colección de calificaciones de estudiantes, todas las respuestas deben de colocarse en un archivo llamado tarea_1.js y debe subirse a gitlab (siendo público el repositorio), utilizando comentarios, deben colocarse las preguntas, luego los comandos utilizados y de nuevo en comentarios las respuestas, al finalizar debe entregarse la liga de gitlab con la tarea.

// 1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

// Comandos
docker start mongo-22
docker cp grades.json mongo-22:/tmp/grades.json
docker exec -ti mongo-22 /bin/bash
mongoimport -d students -c grades < /tmp/grades.json

// Respuestas
// mongo-22
// 2022-11-09T22:42:03.975+0000    connected to: mongodb://localhost/
// 2022-11-09T22:42:04.056+0000    800 document(s) imported successfully. 0 document(s) failed to import.


// 2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count()

use students
// switched to db students

db.grades.count()
// DeprecationWarning: Collection.count() is deprecated. Use countDocuments or estimatedDocumentCount.
// 800

// ¿Cuántos registros arrojo el comando count?
// 800


// 3) Encuentra todas las calificaciones del estudiante con el id numero 4.

db.grades.find({"student_id":4})
// [
//   {
//     _id: ObjectId("50906d7fa3c412bb040eb587"),
//     student_id: 4,
//     type: 'exam',
//     score: 87.89071881934647
//   },
//   {
//     _id: ObjectId("50906d7fa3c412bb040eb589"),
//     student_id: 4,
//     type: 'homework',
//     score: 5.244452510818443
//   },
//   {
//     _id: ObjectId("50906d7fa3c412bb040eb588"),
//     student_id: 4,
//     type: 'quiz',
//     score: 27.29006335059361
//   },
//   {
//     _id: ObjectId("50906d7fa3c412bb040eb58a"),
//     student_id: 4,
//     type: 'homework',
//     score: 28.656451042441
//   }
// ]


// 4) ¿Cuántos registros hay de tipo exam?

db.grades.find({type:"exam"}).count()
// 200


// 5) ¿Cuántos registros hay de tipo homework?

db.grades.find({type:"homework"}).count()
// 400


// 6) ¿Cuántos registros hay de tipo quiz?

db.grades.find({type:"quiz"}).count()
// 200


// 7) Elimina todas las calificaciones del estudiante con el id numero 3

db.grades.remove({student_id:3})
// DeprecationWarning: Collection.remove() is deprecated. Use deleteOne, deleteMany, findOneAndDelete, or bulkWrite.
// { acknowledged: true, deletedCount: 4 }

// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

db.grades.find({score:75.29561445722392, type:"homework"})
//   [
//     {
//       _id: ObjectId("50906d7fa3c412bb040eb59e"),
//       student_id: 9,
//       type: 'homework',
//       score: 75.29561445722392
//     }
//   ]

// 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

db.grades.update({_id:ObjectId("50906d7fa3c412bb040eb591")}, {$set:{score:100}})
// {
//     acknowledged: true,
//     insertedId: null,
//     matchedCount: 1,
//     modifiedCount: 1,
//     upsertedCount: 0
// }

// 10) A qué estudiante pertenece esta calificación.

db.grades.find({"_id":ObjectId("50906d7fa3c412bb040eb591")},{_id:0,type:0})
// [ { student_id: 6, score: 100 } ]